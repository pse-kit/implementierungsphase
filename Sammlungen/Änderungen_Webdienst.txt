- zyklische Abh�ngigkeiten gefixt: alle Klassen in einem Modul
- mehr Attribute mitgeschickt (z.B. deletable_status, can_sign) -> einfacher f�r Frontend/Backend
- optionale Felder m�glich
- Kommentare umgesetzt (war Kann-Kriterium)
- file_handler ausgelagert
- (bei get kann kein JSON mitgeschickt werden -> Parameter)
- handlen von Files (auch bei tasks_fields) (keine echte �nderung, war aber nicht so genau spezifiziert)
- einzelne Spezifikationen (wann darf ein User signieren/wie etc.)

ziemlich unwichtig: 
- Field_type Button gibts nicht mehr
- Statuscodes 
- (viele Hilfsmethoden)
- fast alles wird in Schemas validiert